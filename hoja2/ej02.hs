module Natural where
data Natural = Cero | SUC Natural deriving (Show)
instance Num Natural where
    Cero + a = a
    (SUC a) + b = SUC(a + b)

    a - Cero = a
    Cero  - a = Cero
    (SUC a) - (SUC b) = a - b

    Cero * a = Cero
    (SUC a) * b = a * b + b

    signum Cero = Cero
    signum SUC(a) = SUC Cero

    abs a = a 

    fromInteger 0 = Cero
    fromInteger a = SUC . fromInteger (a-1)

    -- el factorial esta definido en la hoja uno
    factorial :: (Natural a) => a -> a
    factorial Cero = SUC(Cero)
    factorial (SUC a) = (SUC a) * factorial a
    -- no hace falta definir el (^) por ser instancia de Num
