inits :: [a] -> [[a]]
inits xs = map ($ xs) [take x | x<- [0..length(xs)]]
