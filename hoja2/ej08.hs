import BinTreeADT( BinTree, makeEmpty, isEmpty, insert, remove, inOrder)

data Cjto a = Cj (BinTree a)

makeEmptySet :: (Ord a) => Cjto a
makeEmptySet = Cj makeEmpty

insertToSet :: (Ord a) => a -> Cjto a -> Cjto a
insertToSet x (Cj tree)
    | (elem x xs) = Cj tree
    | otherwise = Cj (insert x tree)
    where xs = inOrder tree

isEmptySet :: Ord a => Cjto a -> Bool
isEmptySet (Cj tree) = isEmpty tree

popFromSet :: Ord a => a -> Cjto a -> Cjto a
popFromSet x (Cj tree) = Cj (remove x tree)

isInSet :: (Ord a) => a -> Cjto a -> Bool
isInSet x (Cj tree) = elem x xs
    where xs = inOrder tree

setToList :: Ord a => Cjto a -> [a]
setToList (Cj tree) = inOrder tree
