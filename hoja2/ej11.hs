quitaDups :: Eq a => [a] -> [a]
quitaDups xs = foldr f [] xs
    where f x [] = [x]
          f x (y:ys)
            | (x==y) = x:ys 
            | otherwise = x:y:ys
