primeroQueCumple :: (a -> Bool) -> [a] -> Maybe a
primeroQueCumple p [] = Nothing
primeroQueCumple p (x:xs) 
    | (p x) = Just x
    | otherwise = primeroQueCumple p xs
