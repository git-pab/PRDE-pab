lines :: String -> [String]
lines [] = 
lines xs 
    | null dr = [iz]
    | otherwise = iz: lines (tail dr) -- tail skips the first element
    where (iz, dr) = span(\='\n') xs 
-- Es recomendable repetir cada una de estas funciones, lines, unlines
-- word unwords 