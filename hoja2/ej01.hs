digitsOf :: (Integral a) => a -> [a]
digitsOf n = getDigit n []
    where getDigit 0 xs = xs
          getDigit n xs = getDigit (n `div` 10) ((n `mod` 10):xs)  
imagenEspecular :: Integral a => a -> a
imagenEspecular n = foldr f 0 (digitsOf n) 
    where f x y = 10*y + x 
