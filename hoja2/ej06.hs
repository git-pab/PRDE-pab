data Pila a = P [a]

pilaVacia :: Pila a 
pilaVacia = P []

esVacia :: Pila a -> Bool
esVacia (P []) = True
esVacia _ = False

apilar :: a -> Pila a -> Pila a
apilar x (P xs) = P (x:xs)

consultar :: Pila a -> a
consultar (P (x:xs)) = x 
 
desapilar :: Pila a -> Pila a
desapilar (P (x:xs)) = (P xs)
