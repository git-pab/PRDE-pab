data Cjto a = Cj [a]

makeEmptySet ::(Eq a) => Cjto a
makeEmptySet = Cj []

insertToSet :: (Eq a) => a -> Cjto a -> Cjto a
insertToSet x (Cj xs)
    | (elem x xs) = Cj xs
    | otherwise = Cj (x:xs)

isEmptySet :: Cjto a -> Bool
isEmptySet (Cj []) = True
isEmptySet _ = False

popFromSet :: (Eq a) => a -> Cjto a -> Cjto a
popFromSet x (Cj xs) = Cj xs'
    where xs' = filter (/= x) xs

isInSet :: (Eq a) => a -> Cjto a -> Bool
isInSet x (Cj xs) = elem x xs

setToList :: Cjto a -> [a]
setToList (Cj x) = x
