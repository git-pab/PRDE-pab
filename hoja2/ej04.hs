module BinTreeADT( BinTree, makeEmpty, isEmpty, insert, remove, inOrder) where
data BinTree t = EmptyTree | Leaf t (BinTree t) (BinTree t)

makeEmpty :: Ord t => BinTree t
makeEmpty = EmptyTree

isEmpty :: Ord t => BinTree t -> Bool
isEmpty EmptyTree = True
isEmpty _ = False

insert :: Ord t => t -> BinTree t -> BinTree t
insert x EmptyTree = Leaf x EmptyTree EmptyTree
insert x (Leaf y left rght)
    | (x < y) = Leaf y (insert x left) rght
    | otherwise = Leaf y left (insert x rght) 

remove :: Ord t => t -> BinTree t -> BinTree t
remove x EmptyTree = EmptyTree
remove x (Leaf y left rght)  
    | (x < y) = Leaf y (remove x left) rght
    | (x > y) = Leaf y left (remove x rght)
    | (isEmpty left) = rght
    | (isEmpty rght) = left
    | otherwise = Leaf inSucc' left (remove inSucc' rght)
    where inSucc' = inSucc rght

inSucc :: (Ord t) => BinTree t -> t
inSucc (Leaf x le ri)  
    | (isEmpty le) = x
    | otherwise = inSucc le

preOrder :: (Ord t) => BinTree t -> [t]
preOrder EmptyTree = []
preOrder (Leaf x le ri) = x:(preOrder le) ++ (preOrder ri) 

inOrder :: (Ord t) => BinTree t -> [t]
inOrder EmptyTree = []
inOrder (Leaf x le ri) = (inOrder le) ++ [x] ++ (inOrder ri) 

postOrder :: (Ord t) => BinTree t -> [t]
postOrder EmptyTree = []
postOrder (Leaf x le ri) = (postOrder le) ++ (postOrder ri) ++ [x]

