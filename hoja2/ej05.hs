import BinTreeADT( BinTree, makeEmpty, isEmpty, insert, remove, inOrder)

treeSort :: Ord t => [t] -> [t]
treeSort x = inOrder generatedTree
    where generatedTree = foldr insert makeEmpty x
