module Temp where
data Temp = Kelvin Float | Celsius Float | Fahrenheit Float
    deriving (Show,Read)
zeroKel :: Float
zeroKel = 273.15

toKelvin :: Temp -> Temp
toKelvin (Celsius t) = Kelvin (t + zeroKel)
toKelvin (Fahrenheit t) = Kelvin (( (t - 32) / 1.8 ) + zeroKel)
toKelvin t = t

toCelsius :: Temp -> Temp
toCelsius (Kelvin t) = Celsius (t - zeroKel)
toCelsius (Fahrenheit t) = Celsius ( (t - 32) / 1.8 )
toCelsius t = t

toFahrenheit :: Temp -> Temp
toFahrenheit (Kelvin t) = Fahrenheit ( (32 + (t-zeroKel) * 1.8) )
toFahrenheit (Celsius t) = Fahrenheit ( (32 + t * 1.8) )
toFahrenheit t = t

getUnit :: Temp -> String
getUnit (Kelvin t) = "Kelvin"
getUnit (Celsius t) = "Celsius"
getUnit (Fahrenheit t) = "Fahrenheit"
instance Eq Temp where
    t1 == t2 = let Kelvin t1' = toKelvin t1
                   Kelvin t2' = toKelvin t2
                   in  t1' == t2'
    
instance Ord Temp where
    compare t1 t2 = let Kelvin t1' = toKelvin t1
                        Kelvin t2' = toKelvin t2
                        in  compare t1' t2'
