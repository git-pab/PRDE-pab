takeLast :: Int -> [a] -> [a]
takeLast n x = take n (reverse x)
takeLast2 :: Int -> [a] -> [a]
takeLast2 n x = take n (flipin x)
    where flipin x = go x []
          go [] y = y
          go (r:rs) y = go rs (r:y) 
