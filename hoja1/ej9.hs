dotprod :: (Num t) => [t] -> [t] -> t
dotprod [] a = 0
dotprod a [] = 0
dotprod (x:xs) (y:ys) = x*y + dotprod xs ys 
