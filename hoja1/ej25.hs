gcdList :: (Integral a) => [a] -> a
gcdList = foldr gcd 0 
lcmList :: (Integral a) => [a] -> a
lcmList = foldr lcm 1
