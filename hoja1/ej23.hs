import RationalAsTuple
(/^/) :: Integral a => (a, a) -> a -> (a, a)
(a, b) /^/ 0 = (1,1)
(a, b) /^/ n = if n<0 
    then simpR d' 
    else simpR d
        where d = (a, b) /*/ ( (a, b) /^/ (n-1))
              d'= (b, a) /*/ ( (b, a) /^/ ((-n)-1))
