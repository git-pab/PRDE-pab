module RationalAsTuple where
mapTuple :: (a -> b) -> (a, a) -> (b, b)
mapTuple f (a1, a2) = (f a1, f a2)
simpR (a,b) = if b < 0 
    then simpR' (-a, -b)
    else simpR' (a, b) 
simpR' (a,b) = mapTuple (`div` d) (a,b)
    where d = gcd a b 
(/+/) :: Integral a => (a, a) -> (a, a) -> (a, a)
(a,b) /+/ (c,d) = simpR (a*d + c*d, b*d)
(/-/) :: Integral a => (a, a) -> (a, a) -> (a, a)
(a,b) /-/ (c,d) = simpR (a*d - c*d, b*d)
(/*/) :: Integral a => (a, a) -> (a, a) -> (a, a)
(a,b) /*/ (c,d) = simpR (a*c, b*d)
(///) :: Integral a => (a, a) -> (a, a) -> (a, a)
(a,b) /// (c,d) = simpR (a*d,b*c)
