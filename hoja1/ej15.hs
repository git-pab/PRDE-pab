bin_size :: (Integral a) => a -> a
bin_size x = 1 + bin_size' y
    where y = abs x
bin_size' x 
  | x < 2 = 1
  | otherwise = 1 + bin_size' (x `div` 2)
