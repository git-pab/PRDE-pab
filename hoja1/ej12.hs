import Data.List (sort)
is_ordered :: (Ord t) => [t] -> Bool
is_ordered t = (sort t == t)
