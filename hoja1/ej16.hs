toBin :: Integral a => a -> [a]
toBin 0 = [0]
toBin n = binTransform (abs n) []
    where binTransform 0 r = r
          binTransform k rs = binTransform (k `div` 2) (k `mod` 2:rs)
