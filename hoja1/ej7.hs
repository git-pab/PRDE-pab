myzip3 :: [a] -> [b] -> [c] -> [(a,b,c)]
myzip3 a b [] = []
myzip3 a [] b = []
myzip3 [] a b = []
myzip3 (a:as) (b:bs) (c:cs) = (a,b,c) : myzip3 as bs cs 
