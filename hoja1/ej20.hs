fibonacci :: Int -> Int
fibonacci n = genFib 1 1 !! (n-1)
genFib :: Int -> Int -> [Int]
genFib m n = m : genFib n (m+n)
