filter_even :: (Integral a) => [a] -> [a]
filter_even x = filter even x
filter_odd :: (Integral a) => [a] -> [a]
filter_odd x = filter odd x
alterna :: [a] -> [a] -> [a]
alterna [] [] = []
alterna (x:xs) (y:ys) = x:y:alterna xs ys
coloca :: (Integral a) => [a] -> [a]
coloca x = alterna (filter_even x) (filter_odd x) 
