is_perf_sq :: ( Integral a ) => a -> Bool
is_perf_sq n = ((fromIntegral . round $ sqn) == sqn)
        where sqn = sqrt . fromIntegral $ n
