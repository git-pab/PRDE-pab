dropLast :: Int -> [a] -> [a]
dropLast n x = reverse (drop n (reverse x))
