filter_even :: (Integral a) => [a] -> [a]
filter_even x = filter even x
filter_odd :: (Integral a) => [a] -> [a]
filter_odd x = filter odd x
