minimo :: Ord a => [a] -> a
minimo [a] = a
minimo (a:b:cola)
    | a <= b = minimo (a:cola)
    | otherwise = minimo (b:cola)
maximo :: Ord a => [a] -> a
maximo [a] = a
maximo (a:b:cola)
    | a >= b = maximo (a:cola)
    | otherwise = maximo (b:cola)
