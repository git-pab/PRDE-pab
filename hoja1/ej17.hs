ordSelection :: (Ord t) => [t] -> [t]
ordSelection [] = []
ordSelection x = minVal : (ordSelection xMenosMin)
    where minVal = minimo x
          minimo [a] = a
          minimo (a:b:cola)
            | a <= b = minimo (a:cola)
            | otherwise = minimo (b:cola)   
          xMenosMin = quitar minVal x
          quitar p [] = []
          quitar p (y:ys)
            | p == y = ys
            | otherwise = y:(quitar p ys)
ordQuick [] = []
ordQuick (x:xs) = ordQuick smaller ++ equal ++ ordQuick bigger
    where smaller = filter (<x) xs
          equal = x:filter (==x) xs
          bigger = filter (>x) xs
