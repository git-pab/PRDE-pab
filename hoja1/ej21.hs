mygcd :: Integral a => a -> a -> a
mygcd a 0 = a
mygcd 0 a = a
mygcd a b = mygcd n (m `mod` n)
    where m = max a b
          n = min a b
mylcm :: Integral a => a -> a -> a
mylcm a b = (a*b) `div` (mygcd a b)
