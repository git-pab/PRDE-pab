nroots :: (Num a, Ord a, Integral x)  => a -> a -> a -> x
nroots a b c 
        | disc == 0 = 1
        | disc > 0 = 2
        | otherwise = 0
        where disc = b*b - 4*a*c  
