:- dynamic(hombre/1).
hombre(homer).
hombre(abe).
hombre(bart).
hombre(herb).
:-dynamic(mujer/1).
mujer(marge).
mujer(lisa).
mujer(patty).
mujer(shelma).
mujer(jacqueline).
mujer(mona).
mujer(maggie).

progenitor(abe, homer).
progenitor(mona, homer).
progenitor(abe, herb).
progenitor(mona, herb).
progenitor(homer, bart).
progenitor(marge, bart).
progenitor(homer, lisa).
progenitor(marge, lisa).
progenitor(homer, maggie).
progenitor(marge, maggie).
progenitor(clancy, marge).
progenitor(jacqueline, marge).
progenitor(clancy, patty).
progenitor(jacqueline, patty).
progenitor(clancy, shelma).
progenitor(jacqueline, shelma).

:- dynamic (casados/2).

/* Ejercicio 1 */
squareThat(X, Y) :-
    Y is X*X.

isGt1(X) :-
    X > 1.

map2(_, [], []).
map2( F, [X|Xs], [Y|Ys]) :- 
    call( F, X, Y),
    map2( F, Xs, Ys).
    
/* El funciona para predicados n-arios 
i.e. filterPab(>(1),[1,2,3],X). devuelve X=[2,3]; false.*/
filter( _, [], []).
filter( Cond, [X|Xs], Ys) :-
    ( call( Cond, X) -> Ys = [X|Zs]; Ys = Zs),
    filter( Cond, Xs, Zs).

/* El funciona para predicados  1-arios unicamente 
i.e. filterFer(isGt1,[1,2,3],X). devuelve X=[2,3]; false.*/
filterFer(P,[X|Xs],[X|Ys]):-Obj=..[P,X], call(Obj), !, filterFer(P,Xs,Ys).
filterFer(P,[_|Xs],Ys):- filterFer(P,Xs,Ys).
filterFer(_,[],[]). 

/* El funciona para predicados  n-arios pero he tenido que revertir orden(???)
i.e. filterFerMod(>(1),[1,2,3],X). devuelve X=[2,3]; false.*/
filterFerMod(_,[],[]).
filterFerMod(P,[X|Xs],[X|Ys]):- call(P,X), !, filterFerMod(P,Xs,Ys).
filterFerMod(P,[_|Xs],Ys):- filterFerMod(P,Xs,Ys).

takeWhile( P, [X|Xs], [X|Ys]) :- 
    call(P,X), !,
    takeWhile( P, Xs, Ys).
takeWhile( _, _, []).

dropWhile( P, [X|Xs], Ys) :-
    call(P, X),!,
    dropWhile(P, Xs, Ys).
dropWhile( _, Xs, Xs).

span( P, [X|Xs], [X|Ys], Zs) :-
    call(P, X),
    !,
    span( P, Xs, Ys, Zs).
span( _, Xs, [], Xs).

/* Ejercicio 2 */
atomicos(X,[X]) :-
    var(X),
    !.
atomicos(X,[X]) :-
    atomic(X),
    !.
atomicos(T,L) :-
    compound_name_arguments(T,_,Xs),
    map2(atomicos,Xs,Ls),
    concat(Ls,L).

concat([],[]).
concat([Xs|Xss],Ys):-concat(Xss,Zs),append(Xs,Zs,Ys).

/* Ejercicio 3 */

antepasado(Padre, Hijo) :-
    progenitor(Padre, Hijo).
antepasado(Antepasado, Descendiente) :-
    progenitor(Antepasado_padre, Descendiente),
    antepasado(Antepasado, Antepasado_padre).

descendiente(Descendiente, Antepasado) :-
    antepasado(Antepasado, Descendiente).
pariente(Antepasado, Descendiente) :-
    antepasado(Antepasado, Descendiente).
pariente(Descendiente, Antepasado) :-
    descendiente(Descendiente, Antepasado).
pariente(Pariente1, Pariente2) :-
    antepasado(Antepasado_comun, Pariente1),
    antepasado(Antepasado_comun, Pariente2),
    Pariente1 \== Pariente2.

lista_parientes(X,L) :- 
    setof(Y,pariente(X,Y),L).

/* Ejercicio 4 */

pregunta_conyuje(A) :-
    write('Introduce un conyuje: '),
    read(A).

boda :-
    pregunta_conyuje(A),
    pregunta_conyuje(B),
    assert(casados(A,B)),
    assert(casados(B,A)).

divorcio :-
    pregunta_conyuje(A),
    pregunta_conyuje(B),
    retractall(casados(A,B)),
    retractall(casados(B,A)).
    
/* Ejercicio 5 */

soltero(X) :-
    hombre(X),
    \+casados(X,_).

soltera(X) :-
    mujer(X),
    \+casados(X,_).

/* Ejercicio 6 */

deHombreAMujerTodos :-
    hombre(X),
    retractall(hombre(X)),
    assert(mujer(X)),
    fail.
deMujerAHombreTodos :-
    mujer(X),
    retractall(mujer(X)),
    assert(hombre(X)),
    fail.

cambioSexo(X) :-
    hombre(X),
    retractall(hombre(X)),
    assert(mujer(X)).

cambioSexo(X) :-
    mujer(X),
    retractall(mujer(X)),
    assert(hombre(X)).

doForAll(_,[]).
doForAll(P,[X|Xs]):-
    call(P,X),
    doForAll(P, Xs).

cambioSexoTodos :-
    setof(X,hombre(X),LsH),
    setof(Y,mujer(Y),LsM),
    append(LsH,LsM,Ls),
    doForAll(cambioSexo,Ls).

/* Ejercicio 7 */

tablaDeMultiplicar(Ls):-
    member(X,Ls),
    member(Y,Ls),
    Z is X*Y,
    write(X*Y=Z),nl,
    fail.

/* Ejercicio 8 */

tablaAHandle(Ls,Handle):-
    member(X,Ls),
    member(Y,Ls),
    Z is X*Y,
    writeln(Handle, X*Y=Z),
    fail.
tablaAFichero( Ls) :-
    read(Filename),
    open(Filename, write, Handle),
    tablaAHandle(Ls, Handle),
    close(Handle).

