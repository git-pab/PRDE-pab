hombre(juan).
hombre(javier).
hombre(eusebio).
mujer(pepis).
mujer(rogelia).
progenitor(adan,_).
progenitor(eva,_).
progenitor(pepis, paquito).
progenitor(pepis, pepito).
progenitor(juan, paquito).
progenitor(juan, pepito).
progenitor(javier, firulais).
progenitor(eusebio,juan).
progenitor(eusebio,javier).
progenitor(rogelia,juan).
progenitor(rogelia,javier).
esPadre(X, Y):- 
    hombre(X),
    progenitor(X,Y).

esMadre(X, Y):-
    mujer(X),
    progenitor(X,Y).

sonHermanes(_h1,_h2):- 
    esPadre(X,_h1),
    esPadre(X,_h2),
    esMadre(Y,_h1),
    esMadre(Y,_h2).

esTie(_t,_s):- 
    progenitor(_p, _s),
    sonHermanes(_t,_p).

sonPrimes(_p1, _p2):-
    esTie(_t1,_p2),
    progenitor(_t1,_p1),
    esTie(_t2,_p1),
    progenitor(_t2,_p2).

esAntepasado(_a,_d):-
    progenitor(_a,_d).

esAntepasado(_a,_d):-
	progenitor(_p,_d),
	esAntepasado(_a,_p).

esDescendiente(_d,_a):-
    esAntepasado(_a,_d).

sonParientes(_p1,_p2):-
    esAntepasado(_a,_p1),
    esAntepasado(_a,_p2).
