hombre(homer).
hombre(abe).
hombre(bart).
hombre(herb).

mujer(marge).
mujer(lisa).
mujer(patty).
mujer(shelma).
mujer(jacqueline).
mujer(mona).
mujer(maggie).

progenitor(abe, homer).
progenitor(mona, homer).
progenitor(abe, herb).
progenitor(mona, herb).
progenitor(homer, bart).
progenitor(marge, bart).
progenitor(homer, lisa).
progenitor(marge, lisa).
progenitor(homer, maggie).
progenitor(marge, maggie).
progenitor(clancy, marge).
progenitor(jacqueline, marge).
progenitor(clancy, patty).
progenitor(jacqueline, patty).
progenitor(clancy, shelma).
progenitor(jacqueline, shelma).

arista(a,b).
arista(b,c).
arista(b,d).
arista(c,e).

arista(a,b,2).
arista(b,c,3).
arista(b,d,2).
arista(c,e,4).

/* Ejercicio 1 */

padre(Padre, Hijo) :- 
    progenitor(Padre, Hijo),
    hombre(Padre).
    
madre(Madre, Hijo) :-
    progenitor(Madre, Hijo),
    mujer(Madre).

hermanos(Hermano1,Hermano2) :-
    progenitor(Progenitor1, Hermano1),
    progenitor(Progenitor1, Hermano2),
    progenitor(Progenitor2, Hermano1),
    progenitor(Progenitor2, Hermano2),
    Hermano1 \= Hermano2,
    Progenitor1 @< Progenitor2,
    Progenitor1 \= Progenitor2.

tio(Tio,Sobrino) :-
    hermanos(Tio,Progenitor),
    progenitor(Progenitor, Sobrino).

antepasado(Padre, Hijo) :-
    progenitor(Padre, Hijo).
antepasado(Antepasado, Descendiente) :-
    progenitor(Antepasado_padre, Descendiente),
    antepasado(Antepasado, Antepasado_padre).

descendiente(Descendiente, Antepasado) :-
    antepasado(Antepasado, Descendiente).
    
pariente(Antepasado, Descendiente) :-
    antepasado(Antepasado, Descendiente).
pariente(Descendiente, Antepasado) :-
    descendiente(Descendiente, Antepasado).
pariente(Pariente1, Pariente2) :-
    antepasado(Antepasado_comun, Pariente1),
    antepasado(Antepasado_comun, Pariente2),
    Pariente1 \= Pariente2.

/* Ejercicio 2 */

camino(X,Y) :-
    arista(X,Y).
camino(X,Z) :-
    arista(X,Y),
    camino(Y,Z).

/* Ejercicio 3 */

camino(X,Y,[]) :-
    arista(X,Y).
camino(X,Z,[Y|Ys]) :-
    arista(X,Y),
    camino(Y,Z,Ys).
    
/* Ejercicio 5 */

inversa(Xs,Ys) :-
    inversaAccum(Xs,[],Ys).
inversaAccum([],Xs,Xs).
inversaAccum([X|Xs],Ys,Zs) :-
    inversaAccum(Xs,[X|Ys],Zs).

esPrefijo([],_).
esPrefijo([X|Xs],[X|Ys]) :-
    esPrefijo(Xs,Ys).
  
esSubfijo([],_).
esSubfijo(Xs,Ys) :- 
    inversa(Xs,X_Is),
    inversa(Ys,Y_Is),
    esPrefijo(X_Is,Y_Is).

/* Ejercicio 6 */

isElem(Elem,[Elem|_]).
isElem(Elem,[_|Xs]) :-
    isElem(Elem,Xs).

removeAll(_, [], []).
removeAll(Q,[Q|Ys],Xs) :- 
    removeAll(Q,Ys,Xs).
removeAll(Q,[Y|Ys],[Y|Xs]) :- 
    Q \== Y,
    removeAll(Q,Ys,Xs).

remove(_, [], []).
remove(X,[X|Xs],Xs).
remove(Q,[X|Xs],[X|Ys]):-
    Q\==X,
    remove(Q, Xs, Ys). 

/* Ejercicio 7 */

nub([],[]).
nub([X],[X]).
nub([X,X|Xs],[X|Ys]):-
    nub([X|Xs],[X|Ys]).
nub([X,Y|Xs],[X|Ys]):-
    X\==Y,
    nub([Y|Xs],Ys).
    
/* Ejercicio 8 */

concatenate([],X,X).
concatenate([X|Xs],Y,[X|Zs]):-
    concatenate(Xs,Y,Zs).

concat([],[]).
concat([Xs|Xss],Ys) :-
    concat(Xss,Zs),
    concatenate(Xs,Zs,Ys).
/* Ejercicio 9 */

preOrder(aVacio,[]).
preOrder(nodo(Elemento,Iz,Dr),[Elemento|Xs]):-
    preOrder(Iz,Xs1),
    preOrder(Dr,Xs2),
    append(Xs1,Xs2,Xs).

inOrder(aVacio,[]).
inOrder(nodo(Elemento,Iz,Dr),Xs):-
    inOrder(Iz,Xs1),
    inOrder(Dr,Xs2),
    append(Xs1,[Elemento|Xs2],Xs).

postOrder(aVacio,[]).
postOrder(nodo(Elemento,Iz,Dr),Xs):-
    postOrder(Iz,Xs1),
    postOrder(Dr,Xs2),
    append(Xs2,[Elemento],Xs3),
    append(Xs1,Xs3,Xs).
    
/* Ejercicio 10 */

take2(cero,_,[]).
take2(suc(_),[],[]).
take2(suc(N),[X|Xs],[X|Ys]):-
    take2(N,Xs,Ys).

drop2(cero,X,X).
drop2(suc(_),[],[]).
drop2(suc(N),[_|Xs],Ys):-
    drop(N,Xs,Ys).

splitAt2(cero,Xs,[],Xs).
splitAt2(suc(_),[],[],[]).
splitAt2(suc(N),[X|Xs],[X|Xs1],Xs2):-
    splitAt2(N,Xs,Xs1,Xs2).

/* Ejercicio 11 */
/* como hacerlo funcionar para que take(X,[a,b],[a]) de X = 1? */ 
take(0,_,[]).
take(_,[],[]).
take(N,[X|Xs],[X|Ys]) :-
    N > 0,
    N1 is N-1,
    take(N1, Xs, Ys).

drop(0, X, X).
drop(_, [], []).
drop(N, [_|Xs], Ys):-
    N > 0,
    N1 is N-1,
    drop( N1, Xs, Ys). 

splitAt(0,X,[],X).
splitAt(_,[],[],[]).

/* Ejercicio 12 */

mcd(A,A,A).
mcd(A,B,C) :-
    A > B,
    mcdGt(A,B,C).
mcd(A,B,C) :-
    B > A,
    mcdGt(B,A,C).
mcdGt(A,0,A).
mcdGt(A,B,C) :-
    A > B,
    B > 0,
    AModB is A mod B,
    mcd(B,AModB,C).

/* Ejercicio 13 */

camino( X, Y, [], N) :-
    arista( X, Y, N).
camino( X, Z, [X|Xs], M) :-
    arista( X, Y, N1),
    camino( Y, Z, Xs, N2),
    M is N1+N2.

/* Ejercicio 14 */

factorial( 0, 1).
factorial( N, X) :-
    N > 0,    
    N1 is N-1,
    factorial(N1,X1),
    X is N*X1.

sum([],0).
sum([X|Xs],N):-
    sum(Xs,N1),
    N is X+N1.

max([X],X).
max([X, Y|Xs], X) :-
    X >= Y,
    max( [X|Xs], X).
max( [X|Xs], Z) :-
    max( Xs, Z),
    Z >= X.

dotProd( [], [], 0).
dotProd( [X|Xs], [Y|Ys], Z) :-
    Z1 is X*Y,
    dotProd( Xs, Ys, Z2),
    Z is Z1+Z2.
    
addMatrix( [], [], []).
addMatrix( [Xs|Xss], [Ys|Yss], [Zs|Zss]) :-
    addVector( Xs, Ys, Zs),
    addMatrix( Xss, Yss, Zss).

addVector( [], [], []).
addVector( [X|Xs], [Y|Ys], [Z|Zs]) :-
    Z is X+Y,
    addVector( Xs, Ys, Zs).

/* Ejercicio 15 */
splitByGeq(_,[],[],[]).
splitByGeq(Q, [X|Xs], [X|Ys], Zs) :-
    Q > X,
    splitByGeq( Q, Xs, Ys, Zs).
splitByGeq(Q, [X|Xs], Ys, [X|Zs]) :-
    Q =< X,
    splitByGeq( Q, Xs, Ys, Zs).

quicksort( [], []).
quicksort( [X|Xs], Ys) :-
    splitByGeq( X, Xs, X1, X2),
    quicksort(X1, Y1),
    quicksort(X2, Y2),
    append(Y1,[X|Y2],Ys).
    
/* Ejercicio 16 */

